package tuenti.challenge;

import com.google.common.collect.Maps;
import com.google.common.hash.Hashing;
import com.google.common.primitives.Longs;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Passwords {

    private static String fileName = "Passwords-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;

    private static HashMap<LocalDate, List<Long>> secrets = Maps.newHashMapWithExpectedSize(1785);

    // AS THIS IS FAST ENOUGH WE DO IT EVERY TIME
    private static void loadSecrets() throws IOException, GitAPIException {
        try (Repository repository = new FileRepositoryBuilder().setGitDir(new File("passwords-repo/repo/.git")).build()) {
            try (Git git = new Git(repository)) {
                Iterable<RevCommit> commits = git.log().all().call();
                for (RevCommit commit : commits) {
                    long commitTime = commit.getCommitTime();
                    LocalDate localDate = Instant.ofEpochSecond(commitTime).atZone(ZoneId.systemDefault()).toLocalDate();
                    RevTree tree = commit.getTree();
                    // NOW TRY TO FIND A SPECIFIC FILE
                    try (TreeWalk treeWalk = new TreeWalk(repository)) {
                        treeWalk.addTree(tree);
                        treeWalk.setRecursive(true);
                        treeWalk.setFilter(PathFilter.create("script.php"));
                        if (!treeWalk.next()) {
                            throw new IllegalStateException("Did not find expected file 'script.php'");
                        }
                        ObjectId objectId = treeWalk.getObjectId(0);
                        ObjectLoader loader = repository.open(objectId);
                        // AND THEN ONE CAN THE LOADER TO READ THE FILE
                        String fileContent = new String(loader.getBytes());
                        Pattern pattern = Pattern.compile("^.*\\$secret1 = (?<secret1>\\d+);.*\\$secret2 = (?<secret2>\\d+);.*$", Pattern.DOTALL + Pattern.MULTILINE);
                        Matcher matcher = pattern.matcher(fileContent);
                        boolean matches = matcher.matches();
                        if (!matches) {
                            throw new RuntimeException("UNABLE TO GET SECRETS");
                        }
                        String secret1 = matcher.group("secret1");
                        String secret2 = matcher.group("secret2");
                        secrets.put(localDate, Longs.asList(Long.valueOf(secret1), Long.valueOf(secret2)));
                    }
                }
            }
        }
    }

    public static void main(String[] args) throws IOException, GitAPIException {
        Locale.setDefault(Locale.ENGLISH);
        loadSecrets();
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ": ");
            System.out.println("Case #" + t + ": ");
            new Passwords().solve();
            out.println();
            out.flush();
        }
        in.close();
        out.close();
    }

    private void solve() throws IOException {
        String userId = in.next();
        int numEntries = in.nextInt();
        in.nextLine();
        String[] passwordInfo = new String[]{userId, ""};
        LocalDate lastDate = LocalDate.MIN;
        for (int entry = 0; entry < numEntries; entry++) {
            String D = in.next();
            int C = in.nextInt();
            LocalDate date = LocalDate.parse(D);
            if (date.isBefore(lastDate)) {
                throw new RuntimeException("DATES ARE NOT ORDERED");
            }
            lastDate = date;
            for (int changes = 0; changes < C; changes++) {
                passwordInfo = generatePassword(passwordInfo[0], passwordInfo[1], date);
            }
        }
        out.print(passwordInfo[0]);
    }

    private String[] generatePassword(String userId, String oldHash, LocalDate date) throws IOException {
        List<Long> secrets = obtainSecrets(date);
        long secret1 = secrets.get(0);
        long secret2 = secrets.get(1);
        long secret3;
        if (oldHash.isEmpty()) {
            secret3 = Hashing.crc32().hashString(userId, StandardCharsets.UTF_8).padToLong();
        } else {
            secret3 = Hashing.crc32().hashString(oldHash, StandardCharsets.UTF_8).padToLong();
        }
        long counter = secret3;
        counter = (counter * modPow(secret1, 10000000, secret2)) % secret2;
        StringBuilder password = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            counter = (counter * secret1) % secret2;
            password = password.append(String.valueOf((char) ((counter % 94) + 33)));
        }
        String hash = Hashing.md5().hashString(password, StandardCharsets.UTF_8).toString();
        return new String[]{password.toString(), hash};
    }

    // Schneier, Bruce (1996). Applied Cryptography: Protocols, Algorithms, and Source Code in C, Second Edition (2nd ed.). Wiley. ISBN 978-0-471-11709-4.
    // WITHOUT THIS IT TAKES FOREVER, WITH IT IS INSTANTANEOUS
    private long modPow(long base, long exp, long modulus) {
        base %= modulus;
        long result = 1L;
        while (exp > 0) {
            if ((exp & 1L) == 1L) {
                result = (result * base) % modulus;
            }
            base = (base * base) % modulus;
            exp >>= 1;
        }
        return result;

    }

    private List<Long> obtainSecrets(LocalDate date) throws IOException {
        List<Long> secretValues = secrets.get(date);
        if (secretValues != null) {
            return secretValues;
        }
        date = date.minusDays(1);
        throw new RuntimeException("UNABLE TO LOOKUP SECRETS FOR DATE " + date);
    }
}
