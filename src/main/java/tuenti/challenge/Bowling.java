package tuenti.challenge;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

public class Bowling {

    private static String fileName = "Bowling-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ":");
            new Bowling().solve();
            out.println();
        }
        in.close();
        out.close();
    }

    private void solve() {
        int R = in.nextInt();
        int[] rolls = new int[R];
        for (int roll = 0; roll < R; roll++) {
            rolls[roll] = in.nextInt();
        }
        in.nextLine();
        int score = 0;
        int frame = 0;
        for (int i = 0; i < 10; i++) {
            if (rolls[frame] == 10) {
                score += 10 + rolls[frame + 1] + rolls[frame + 1 + 1];
                frame++;
            } else if (rolls[frame] + rolls[frame + 1] == 10) {
                score += 10 + rolls[frame + 2];
                frame += 2;
            } else {
                score += rolls[frame] + rolls[frame + 1];
                frame += 2;
            }
            out.print(" ");
            out.print(score);
        }
    }

}
