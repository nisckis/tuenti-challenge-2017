package tuenti.challenge;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Unicode {

    private static String fileName = "Unicode-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
//        in = new Scanner(new File(inputFileName), Charsets.UTF_16LE.name());
//        String line = in.nextLine();
//        int tests = Integer.valueOf(line);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ": ");
            new Unicode().solve();
            out.println();
        }
        in.close();
        out.close();
    }

    private void solve() {
        String line = in.nextLine().trim();
        List<String> values = Splitter.on(CharMatcher.breakingWhitespace()).trimResults().omitEmptyStrings().splitToList(line);
        System.out.println(values);
        if (values.size() > 1) {
            out.print("N/A");
            return;
        }
        try {
            BigInteger value = new BigInteger(values.get(0));
            out.print(value.toString(16));
        } catch (NumberFormatException e) {
            out.print("N/A");
        }
    }
}
