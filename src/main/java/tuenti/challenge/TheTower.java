package tuenti.challenge;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Sets;
import com.google.common.collect.TreeBasedTable;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.*;

public class TheTower {

    private static String fileName = "TheTower-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;

    private static final long ZERO = 0L;
    private static final long ONE = 1L;

    private class Graph {

        Set<Node> nodes = new HashSet<>();

        void addNode(Node nodeA) {
            nodes.add(nodeA);
        }

        @Override
        public String toString() {
            return nodes.toString();
        }
    }

    private class Node {

        long name;
        List<Node> shortestPath = new LinkedList<>();
        long distance = Long.MAX_VALUE;
        Map<Node, Long> adjacentNodes = new HashMap<>();

        void addDestination(Node destination, long distance) {
            adjacentNodes.put(destination, distance);
        }

        Node(long name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this).add("name", name).add("distance", distance).toString();
        }
    }

    private static Graph calculateShortestPathFromSource(Graph graph, Node source, Long target) {
        source.distance = 0;

        Set<Node> settledNodes = new HashSet<>();
        Set<Node> unsettledNodes = new HashSet<>();

        unsettledNodes.add(source);

        while (unsettledNodes.size() != 0) {
            Node currentNode = getLowestDistanceNode(unsettledNodes);
            if (currentNode.name == target) {
                return graph;
            }
            unsettledNodes.remove(currentNode);
            for (Map.Entry<Node, Long> adjacencyPair : currentNode.adjacentNodes.entrySet()) {
                Node adjacentNode = adjacencyPair.getKey();
                long edgeWeight = adjacencyPair.getValue();
                if (!settledNodes.contains(adjacentNode)) {
                    calculateMinimumDistance(adjacentNode, edgeWeight, currentNode);
                    unsettledNodes.add(adjacentNode);
                }
            }
            settledNodes.add(currentNode);
        }
        return graph;
    }

    private static Node getLowestDistanceNode(Set<Node> unsettledNodes) {
        Node lowestDistanceNode = null;
        long lowestDistance = Long.MAX_VALUE;
        for (Node node : unsettledNodes) {
            long nodeDistance = node.distance;
            if (nodeDistance < lowestDistance) {
                lowestDistance = nodeDistance;
                lowestDistanceNode = node;
            }
        }
        return lowestDistanceNode;
    }

    private static void calculateMinimumDistance(Node evaluationNode, long edgeWeigh, Node sourceNode) {
        long sourceDistance = sourceNode.distance;
        if (sourceDistance + edgeWeigh < evaluationNode.distance) {
            evaluationNode.distance = sourceDistance + edgeWeigh;
            LinkedList<Node> shortestPath = new LinkedList<>(sourceNode.shortestPath);
            shortestPath.add(sourceNode);
            evaluationNode.shortestPath = shortestPath;
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ": ");
            System.out.println(t);
            new TheTower().solve();
            out.println();
        }
        in.close();
        out.close();
    }

    private void solve() {
        long F = in.nextLong();
        int S = in.nextInt();
        in.nextLine();
        if (F == ONE) {
            out.print("0");
            return;
        }
        if (S == 0) {
            out.print(sumatorio(F));
            return;
        }
        TreeBasedTable<Long, Long, Long> paths = TreeBasedTable.create();
        for (int shortcut = 0; shortcut < S; shortcut++) {
            long A = in.nextLong();
            long B = in.nextLong();
            long Y = in.nextLong();
            in.nextLine();
            if (A >= B) {
                continue;
            }
            if (Y >= sumatorio(B) - sumatorio(A)) {
                continue;
            }
            Long currentY = paths.get(A, B);
            if ((currentY == null) || (Y < currentY)) {
                paths.put(A, B, Y);
            }
            if (B != F) {
                currentY = paths.get(A, F);
                long cost = sumatorio(F) - sumatorio(A);
                if ((currentY == null) || (cost < currentY)) {
                    paths.put(A, F, cost);
                }
                currentY = paths.get(B, F);
                cost = sumatorio(F) - sumatorio(B);
                if ((currentY == null) || (cost < currentY)) {
                    paths.put(B, F, cost);
                }
            }
        }
        if (paths.isEmpty()) {
            out.print(sumatorio(F));
            return;
        }
        // {3={5=2, 6=3, 9=33}, 4={8=6, 9=30}, 5={9=26}, 6={9=21}, 8={9=8}}
        {
            long currentRow = 1;
            long maxRow = paths.rowKeySet().stream().max(Long::compareTo).orElse(F);
            if (maxRow < F) {
                Long currentCost = paths.get(maxRow, F);
                long cost = sumatorio(F) - sumatorio(maxRow);
                if ((currentCost == null) || (cost < currentCost)) {
                    paths.put(maxRow, F, cost);
                }
            }
            while (currentRow < maxRow) {
                long finalCurrentRow = currentRow;
                long first = paths.rowKeySet().stream().filter(row -> row > finalCurrentRow).findFirst().orElse(Long.MAX_VALUE);
                if (first < Long.MAX_VALUE) {
                    long cost = sumatorio(first) - sumatorio(currentRow);
                    currentRow = first;
                    Long currentCost = paths.get(finalCurrentRow, first);
                    if ((currentCost == null) || (cost < currentCost)) {
                        paths.put(finalCurrentRow, first, cost);
                    }

                } else {
                    currentRow++;
                }
            }
        }
        // {1={3=3}, 3={4=3, 5=2, 6=3, 9=33}, 4={5=4, 8=6, 9=30}, 5={6=5, 9=26}, 6={8=13, 9=21}, 8={9=8}}
        Graph graph = new Graph();
        Node source;
        Node dest;
        {
            Long[] rows = Sets.union(paths.rowKeySet(), paths.columnKeySet()).toArray(new Long[0]);
            for (int rowPos = rows.length - 1; rowPos >= 0; rowPos--) {
                long row = rows[rowPos];
                for (int pos = rowPos - 1; pos > 0; pos--) {
                    long previousRow = rows[pos];
                    paths.put(row, previousRow, ZERO);
                }
            }
            // {1={3=3}, 3={4=3, 5=2, 6=3, 9=33}, 4={3=0, 5=4, 8=6, 9=30}, 5={3=0, 4=0, 6=5, 9=26}, 6={3=0, 4=0, 5=0, 8=13, 9=21}, 8={3=0, 4=0, 5=0, 6=0, 9=8}, 9={3=0, 4=0, 5=0, 6=0, 8=0}}
            TreeMap<Long, Node> alreadyCreated = new TreeMap<>();
            for (Long row : rows) {
                Node rowNode = alreadyCreated.get(row);
                if (rowNode == null) {
                    rowNode = new Node(row);
                    alreadyCreated.put(row, rowNode);
                }
                SortedMap<Long, Long> columns = paths.row(row);
                for (Map.Entry<Long, Long> entry : columns.entrySet()) {
                    long column = entry.getKey();
                    long distance = entry.getValue();
                    Node columnNode = alreadyCreated.get(column);
                    if (columnNode == null) {
                        columnNode = new Node(column);
                        alreadyCreated.put(column, columnNode);
                    }
                    rowNode.addDestination(columnNode, distance);
                }
                graph.addNode(rowNode);
            }
            source = alreadyCreated.get(ONE);
            dest = alreadyCreated.get(F);
        }
        calculateShortestPathFromSource(graph, source, F);
        long years = dest.distance;
        out.print(years);
    }

    private long sumatorio(long b) {
        return (b * (b - 1)) >> 1;
    }
}
