package tuenti.challenge;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

public class BoardGames {

    private static String fileName = "BoardGames-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ": ");
            new BoardGames().solve();
            out.println();
        }
        in.close();
        out.close();
    }

    private void solve() {
        double P = in.nextDouble();
        out.print(Math.max((int) Math.ceil(Math.log(P + 1) / Math.log(2.0d)), 1));
    }
}
