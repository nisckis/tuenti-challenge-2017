package tuenti.challenge;

import com.google.common.base.MoreObjects;
import com.google.common.collect.*;
import com.google.common.graph.Graphs;
import com.google.common.graph.ImmutableNetwork;
import com.google.common.graph.MutableNetwork;
import com.google.common.graph.NetworkBuilder;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Colors {

    private static final long UNREACHABLE = -1L;
    private static String fileName = "Colors-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ":");
            System.out.print("Case #" + t + ": ");
            LocalDateTime now = LocalDateTime.now();
            new Colors().solve();
            System.out.println(now.until(LocalDateTime.now(), ChronoUnit.MILLIS));
            out.println();
        }
        in.close();
        out.close();
    }

    private void solve() {
        int K = in.nextInt();
        in.nextLine();
        HashMap<String, Integer> colors = loadColors(K);
        int G = in.nextInt();
        in.nextLine();
        HashBasedTable<Integer, Integer, Integer> galaxies = loadGalaxies(K, G, colors);
        ImmutableNetwork<Integer, HashMap<Integer, Integer>> universe = loadWormHoles(colors, G, galaxies.columnKeySet());
        validateInputData(colors, galaxies, universe);
        HashMap<Integer, Long> minimumCosts = Maps.newHashMapWithExpectedSize(G);
        minimumCosts.put(0, 0L);
        for (int galaxy = 1; galaxy < G; galaxy++) {
            minimumCosts.put(galaxy, Long.MAX_VALUE);
        }
        // MARK UNREACHABLE GALAXIES
        for (Integer unreachableGalaxy : Sets.difference(universe.nodes(), Graphs.reachableNodes(universe.asGraph(), 0))) {
            minimumCosts.put(unreachableGalaxy, UNREACHABLE);
        }
        State initialState = new State(0, 0L, 0);
        TreeBasedTable<Integer, Integer, Long> alreadyProcessed = TreeBasedTable.create();
        PriorityQueue<State> toProcess = new PriorityQueue<>(1024);
//        TreeSet<State> toProcess = new TreeSet<>();
        toProcess.add(initialState);
        while (!toProcess.isEmpty()) {
            // THEORICALLY WITH DIJKSTRA WE CAN STOP ONCE ALL MINIMUM INITIAL VALUES HAVE BEEN MODIFIED
/*
            if (minimumCosts.values().parallelStream().anyMatch(Predicate.isEqual(Long.MAX_VALUE))) {
                break;
            }
*/
            State currentState = toProcess.poll();
//            State currentState = toProcess.pollFirst();
            // BUT WE KNOW FOR SURE THAT IF CURRENT COST IS OVER THE CURRENT MAXIMUM MINIMUM WE HAVE FINISHED
            Long maxValue = minimumCosts.values().parallelStream().max(Long::compareTo).orElse(Long.MAX_VALUE);
            if (currentState.currentCost >= maxValue) {
                break;
            }
            Long previousCost = alreadyProcessed.get(currentState.currentGalaxy, currentState.colors);
            // WE CHECK STRICTLY FOR LESS THAN BECAUSE WE MARK NODES AS PROCESSED BEFOREHAND
            if ((previousCost != null) && (previousCost < currentState.currentCost)) {
                continue;
            }
            if (previousCost == null) {
                // IF WE HAVE A BETTER PARENT USE HIS COST AS OURS
                Long parentCost = getParentCost(alreadyProcessed, currentState);
                if (parentCost <= currentState.currentCost) {
                    alreadyProcessed.put(currentState.currentGalaxy, currentState.colors, parentCost);
                    continue;
                }
            }
            // MOVE TO NEAR GALAXIES IF USEFUL
            moveToNearGalaxies(universe, minimumCosts, alreadyProcessed, toProcess, currentState);
            // GATHER ENERGIES IF USEFUL
            gatherEnergies(galaxies, minimumCosts, alreadyProcessed, toProcess, currentState);
        }
        // PRINT COSTS
        minimumCosts.keySet().stream().sorted().mapToLong(galaxy -> minimumCosts.get(galaxy) == Long.MAX_VALUE ? UNREACHABLE : minimumCosts.get(galaxy)).forEach(cost -> out.print(" " + cost));
        out.flush();
    }

    private Long getParentCost(TreeBasedTable<Integer, Integer, Long> alreadyProcessed, State currentState) {
        Long minimumCost = Long.MAX_VALUE;
        for (Map.Entry<Integer, Long> state : alreadyProcessed.row(currentState.currentGalaxy).entrySet()) {
            Integer color = state.getKey();
            Long cost = state.getValue();
            if (((currentState.colors & color) == currentState.colors) && (minimumCost > cost)) {
                minimumCost = cost;
                // processNewState UPDATES CHILDREN COSTS SO ALL OUR PARENTS ALWAYS HAVE THE SAME COST
                break;
            }
        }
        return minimumCost;
    }

    // TODO THIS IS SUBOPTIMAL BECAUSE WE SHOULD GENERATE ALL GATHERING OPTIONS BEFOREHAND AND MARK THE NEW STATE AS JUST GATHERED SO THESE CAN MOVE ONLY
    // ANYWAY, EVEN AS SUBOPTIMAL EVERY USE CASE RUNS IN SUBSECOND TIME SO IT'S GOOD ENOUGH
    private void gatherEnergies(HashBasedTable<Integer, Integer, Integer> galaxies, HashMap<Integer, Long> minimumCosts, TreeBasedTable<Integer, Integer, Long> alreadyProcessed, Collection<State> toProcess, State currentState) {
        Map<Integer, Integer> currentGalaxy = galaxies.row(currentState.currentGalaxy);
        HashSet<Integer> usefulColors = currentGalaxy.keySet().parallelStream().filter(color -> (color | currentState.colors) != currentState.colors).collect(Collectors.toCollection(() -> Sets.newHashSetWithExpectedSize(currentGalaxy.size())));
        for (Integer usefulColor : usefulColors) {
            int finalColor = currentState.colors | usefulColor;
            long gatheringCost = galaxies.get(currentState.currentGalaxy, usefulColor);
            Long newCost = gatheringCost + currentState.currentCost;
            Long currentCost = alreadyProcessed.get(currentState.currentGalaxy, finalColor);
            if ((currentCost == null) || (currentCost > newCost)) {
                State newState = new State(currentState.currentGalaxy, newCost, finalColor);
                processNewState(minimumCosts, alreadyProcessed, toProcess, newState);
            }
        }
    }

    private void moveToNearGalaxies(ImmutableNetwork<Integer, HashMap<Integer, Integer>> universe, HashMap<Integer, Long> minimumCosts, TreeBasedTable<Integer, Integer, Long> alreadyProcessed, Collection<State> toProcess, State currentState) {
        if (currentState.colors != 0) {
            Set<HashMap<Integer, Integer>> outEdges = universe.outEdges(currentState.currentGalaxy);
            for (HashMap<Integer, Integer> outEdge : outEdges) {
                if (outEdge.entrySet().size() > 1) {
                    throw new RuntimeException("GOT MORE THAN ONE COLOR LINK ENTRY");
                }
                Integer colorsNeeded = outEdge.entrySet().iterator().next().getValue();
                if ((currentState.colors & colorsNeeded) == colorsNeeded) {
                    State newState = new State(universe.incidentNodes(outEdge).target(), currentState.currentCost, currentState.colors ^ colorsNeeded);
                    processNewState(minimumCosts, alreadyProcessed, toProcess, newState);
                }
            }
        }
    }

    private void processNewState(HashMap<Integer, Long> minimumCosts, TreeBasedTable<Integer, Integer, Long> alreadyProcessed, Collection<State> toProcess, State newState) {
        // WE COULD REMOVE toProcess STATES THAT NOW ARE USELESS BECAUSE THIS NEW STATE IS BETTER, BUT FOR THE EXISTING USE CASES THIS IS SLOWER THAN PROCESSING AND DISCARDING THEM
//        toProcess.removeIf(pendingState -> (pendingState.currentGalaxy == newState.currentGalaxy) && ((pendingState.colors | newState.colors) == newState.colors) && (pendingState.currentCost >= newState.currentCost));
        Long previousCost = alreadyProcessed.get(newState.currentGalaxy, newState.colors);
        if ((previousCost != null) && (previousCost <= newState.currentCost)) {
            return;
        }
        SortedMap<Integer, Long> previousGalaxyStates = alreadyProcessed.row(newState.currentGalaxy);
        if (previousGalaxyStates.isEmpty()) {
            toProcess.add(newState);
            alreadyProcessed.put(newState.currentGalaxy, newState.colors, newState.currentCost);
            Long currentMinimumCost = minimumCosts.get(newState.currentGalaxy);
            if ((currentMinimumCost == null) || (currentMinimumCost > newState.currentCost)) {
                minimumCosts.put(newState.currentGalaxy, newState.currentCost);
            }
            return;
        }
        previousGalaxyStates = Maps.newTreeMap(previousGalaxyStates);
        for (Map.Entry<Integer, Long> state : previousGalaxyStates.entrySet()) {
            Integer color = state.getKey();
            Long cost = state.getValue();
            if (((newState.colors & color) == newState.colors) && (newState.currentCost >= cost)) {
                // BETTER PARENT FOUND
                alreadyProcessed.put(newState.currentGalaxy, newState.colors, cost);
                return;
            }
            // IF WE DIDN'T HAVE ANY BETTER PARENT THEN WE WOULD HAVE UPDATED ALL OUR CHILDREN ACCORDING
            if (((newState.colors & color) == color) && (newState.currentCost < cost)) {
                // WE ARE A BETTER PARENT
                alreadyProcessed.put(newState.currentGalaxy, color, newState.currentCost);
            }
        }
        toProcess.add(newState);
        alreadyProcessed.put(newState.currentGalaxy, newState.colors, newState.currentCost);
        Long currentMinimumCost = minimumCosts.get(newState.currentGalaxy);
        if ((currentMinimumCost == null) || (currentMinimumCost > newState.currentCost)) {
            minimumCosts.put(newState.currentGalaxy, newState.currentCost);
        }
    }

    private void validateInputData(HashMap<String, Integer> colors, HashBasedTable<Integer, Integer, Integer> galaxies, ImmutableNetwork<Integer, HashMap<Integer, Integer>> universe) {
        Collection<Integer> colorValues = colors.values();
        long count = colorValues.stream().filter(compoundColors -> compoundColors == (compoundColors & -compoundColors)).count();
        if (count > 10) {
            throw new RuntimeException("GOT " + count + " PRIMARY COLORS");
        }
        if (new HashSet<>(colorValues).size() != colorValues.size()) {
            throw new RuntimeException("COMPOSITE COLOR REPEATED");
        }
    }

    private ImmutableNetwork<Integer, HashMap<Integer, Integer>> loadWormHoles(HashMap<String, Integer> colors, int numGalaxies, Set<Integer> gatherableColors) {
        int W = in.nextInt();
        in.nextLine();
        MutableNetwork<Integer, HashMap<Integer, Integer>> paths = NetworkBuilder.directed().allowsSelfLoops(false).allowsParallelEdges(true).expectedNodeCount(numGalaxies)/*.nodeOrder(ElementOrder.<Integer>natural())*/.build();
        // ADD ALL NODES
        IntStream.range(0, numGalaxies).forEach(paths::addNode);
        // ADD EXISTING LINKS
        for (int numWormHole = 0; numWormHole < W; numWormHole++) {
            String color = in.next();
            int A = in.nextInt();
            int B = in.nextInt();
            in.nextLine();
            HashMap<Integer, Integer> wormholeColors = Maps.newHashMapWithExpectedSize(1);
            wormholeColors.put(numWormHole, colors.get(color));
            paths.addEdge(A, B, wormholeColors);
        }
        return ImmutableNetwork.copyOf(paths);
    }

    private HashBasedTable<Integer, Integer, Integer> loadGalaxies(int numColors, int numGalaxies, HashMap<String, Integer> colors) {
        HashBasedTable<Integer, Integer, Integer> galaxies = HashBasedTable.create(numGalaxies, numColors);
        for (int numGalaxy = 0; numGalaxy < numGalaxies; numGalaxy++) {
            long E = in.nextLong();
            for (long numEnergy = 0; numEnergy < E; numEnergy++) {
                String energy = in.next();
                int gatherCost = in.nextInt();
                in.nextLine();
                if (galaxies.put(numGalaxy, colors.get(energy), gatherCost) != null) {
                    throw new RuntimeException("GATHER COLOR ALREADY DEFINED");
                }
            }
        }
        return galaxies;
    }

    private HashMap<String, Integer> loadColors(int k) {
        int primaryColors = 0;
        HashMap<String, Integer> colors = Maps.newHashMapWithExpectedSize(k);
        for (int numColor = 0; numColor < k; numColor++) {
            String color = in.next();
            int numColors = in.nextInt();
            if (numColors == 0) {
                colors.put(color, 1 << primaryColors);
                primaryColors++;
            } else {
                for (int numPrimaryColor = 0; numPrimaryColor < numColors; numPrimaryColor++) {
                    String compoundingColor = in.next();
                    Integer realColors = colors.get(compoundingColor);
                    colors.merge(color, realColors, (oldColor, newColor) -> oldColor | newColor);
                }
            }
            in.nextLine();
        }
        return colors;
    }

    static class State implements Comparable<State> {

        final int currentGalaxy;
        final long currentCost;
        final int colors;

        State(int currentGalaxy, long currentCost, int colors) {
            this.currentGalaxy = currentGalaxy;
            this.currentCost = currentCost;
            this.colors = colors;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this).add("currentGalaxy", currentGalaxy).add("currentCost", currentCost).add("colors", colors).toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            State state = (State) o;
            return (currentGalaxy == state.currentGalaxy) && (currentCost == state.currentCost) && (colors != state.colors);
        }

        @Override
        public int hashCode() {
            int result = currentGalaxy;
            result = 31 * result + (int) currentCost;
            result = 31 * result + colors;
            return result;
        }

        @Override
        public int compareTo(State o) {
            return ComparisonChain.start().compare(this.currentCost, o.currentCost).compare(this.currentGalaxy, o.currentGalaxy).compare(this.colors, o.colors).result();
        }
    }

}