package tuenti.challenge;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

public class PizzaLove {

    private static String fileName = "PizzaLove-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;

    private static final int SLICES_PER_PIZZA = 8;

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ": ");
            new PizzaLove().solve();
            out.println();
        }
        in.close();
        out.close();
    }

    private void solve() {
        int attendingPeople = in.nextInt();
        in.nextLine();
        int numSlices = 0;
        for (int person = 0; person < attendingPeople; person++) {
            numSlices += in.nextInt();
        }
        in.nextLine();
        out.print((numSlices / SLICES_PER_PIZZA) + ((numSlices % SLICES_PER_PIZZA == 0) ? 0 : 1));
    }
}
