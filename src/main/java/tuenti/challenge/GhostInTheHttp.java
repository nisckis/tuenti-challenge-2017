package tuenti.challenge;

import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class GhostInTheHttp {

    private static String fileName = "GhostInTheHttp";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;
    private static FileOutputStream fileOutputStream;

    private static void fastCopy(final InputStream src, final OutputStream dest) throws IOException {
        final ReadableByteChannel inputChannel = Channels.newChannel(src);
        final WritableByteChannel outputChannel = Channels.newChannel(dest);
        fastCopy(inputChannel, outputChannel);
    }

    private static void fastCopy(final ReadableByteChannel src, final WritableByteChannel dest) throws IOException {
        try {
            final ByteBuffer buffer = ByteBuffer.allocateDirect(16 * 1024);
            while (src.read(buffer) != -1) {
                buffer.flip();
                dest.write(buffer);
                buffer.compact();
            }
            buffer.flip();
            while (buffer.hasRemaining()) {
                dest.write(buffer);
            }
        } catch (org.apache.http.ConnectionClosedException | java.net.ProtocolException e) {
            // IGNORE THIS
        }
    }

    public static void main(String[] args) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, IOException {
//        withHttpClient();
        withOkClient();
    }

    private static OkHttpClient getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier((hostname, session) -> true);
            builder.protocols(Arrays.asList(Protocol.HTTP_2, Protocol.HTTP_1_1));
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(logging);

            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static void withOkClient() throws IOException {
        // WE HAVE TO PUT A BREAKING POINT AT LINE 88 FROM okhttp3.internal.http2.PushObserver
        // THE TOKEN IS THE TEXT RECEIVED AS 'source'
        Locale.setDefault(Locale.ENGLISH);
        fileOutputStream = new FileOutputStream(outputFileName);
        out = new PrintWriter(fileOutputStream);
        OkHttpClient client = getUnsafeOkHttpClient();
        Request request = new Request.Builder().url("https://52.49.91.111:8443/ghost")
//            Request request = new Request.Builder().url("https://http2.akamai.com/demo")
                .addHeader(HttpHeaders.RANGE, "bytes=4017-8120")
                .addHeader(HttpHeaders.ACCEPT_ENCODING, "identity")
                .build();
        Response response = client.newCall(request).execute();
        fastCopy(response.body().byteStream(), fileOutputStream);
        fileOutputStream.flush();
        out.println();
        out.flush();
        out.close();
        fileOutputStream.close();
    }

    private static void withHttpClient() throws IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        Locale.setDefault(Locale.ENGLISH);
        fileOutputStream = new FileOutputStream(outputFileName);
        out = new PrintWriter(fileOutputStream);
        for (int pos = 4017; pos < 4018; pos++) {

            try (CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(
                    new SSLConnectionSocketFactory(
                            SSLContexts.custom().loadTrustMaterial(null, (chain, authType) -> true).build(),
                            new String[]{
//                                    "NONE",
//                                    "SSLv2Hello",
                                    "SSLv3",
                                    "TLSv1",
//                                    "TLSv1.1",
//                                    "TLSv1.2"
                            },
                            new String[]{
                                    "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA384",
                                    "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384",
                                    "TLS_RSA_WITH_AES_256_CBC_SHA256",
                                    "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA384",
                                    "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA384",
                                    "TLS_DHE_RSA_WITH_AES_256_CBC_SHA256",
                                    "TLS_DHE_DSS_WITH_AES_256_CBC_SHA256",
                                    "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA",
                                    "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA",
                                    "TLS_RSA_WITH_AES_256_CBC_SHA",
                                    "TLS_ECDH_ECDSA_WITH_AES_256_CBC_SHA",
                                    "TLS_ECDH_RSA_WITH_AES_256_CBC_SHA",
                                    "TLS_DHE_RSA_WITH_AES_256_CBC_SHA",
                                    "TLS_DHE_DSS_WITH_AES_256_CBC_SHA",
                                    "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA256",
                                    "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256",
                                    "TLS_RSA_WITH_AES_128_CBC_SHA256",
                                    "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA256",
                                    "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA256",
                                    "TLS_DHE_RSA_WITH_AES_128_CBC_SHA256",
                                    "TLS_DHE_DSS_WITH_AES_128_CBC_SHA256",
                                    "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA",
                                    "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                                    "TLS_RSA_WITH_AES_128_CBC_SHA",
                                    "TLS_ECDH_ECDSA_WITH_AES_128_CBC_SHA",
                                    "TLS_ECDH_RSA_WITH_AES_128_CBC_SHA",
                                    "TLS_DHE_RSA_WITH_AES_128_CBC_SHA",
                                    "TLS_DHE_DSS_WITH_AES_128_CBC_SHA",
                                    "TLS_ECDHE_ECDSA_WITH_RC4_128_SHA",
                                    "TLS_ECDHE_RSA_WITH_RC4_128_SHA",
                                    "SSL_RSA_WITH_RC4_128_SHA",
                                    "TLS_ECDH_ECDSA_WITH_RC4_128_SHA",
                                    "TLS_ECDH_RSA_WITH_RC4_128_SHA",
                                    "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384",
                                    "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256",
                                    "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384",
                                    "TLS_RSA_WITH_AES_256_GCM_SHA384",
                                    "TLS_ECDH_ECDSA_WITH_AES_256_GCM_SHA384",
                                    "TLS_ECDH_RSA_WITH_AES_256_GCM_SHA384",
                                    "TLS_DHE_RSA_WITH_AES_256_GCM_SHA384",
                                    "TLS_DHE_DSS_WITH_AES_256_GCM_SHA384",
                                    "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
                                    "TLS_RSA_WITH_AES_128_GCM_SHA256",
                                    "TLS_ECDH_ECDSA_WITH_AES_128_GCM_SHA256",
                                    "TLS_ECDH_RSA_WITH_AES_128_GCM_SHA256",
                                    "TLS_DHE_RSA_WITH_AES_128_GCM_SHA256",
                                    "TLS_DHE_DSS_WITH_AES_128_GCM_SHA256",
                                    "TLS_ECDHE_ECDSA_WITH_3DES_EDE_CBC_SHA",
                                    "TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA",
                                    "SSL_RSA_WITH_3DES_EDE_CBC_SHA",
                                    "TLS_ECDH_ECDSA_WITH_3DES_EDE_CBC_SHA",
                                    "TLS_ECDH_RSA_WITH_3DES_EDE_CBC_SHA",
                                    "SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA",
                                    "SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA",
                                    "SSL_RSA_WITH_RC4_128_MD5",
                                    "TLS_EMPTY_RENEGOTIATION_INFO_SCSV"
                            },
                            NoopHostnameVerifier.INSTANCE))
                    .build()) {
//                SSLContext sslContext = SSLContexts.custom()/*.useTLS()*/.build();
//                SSLConnectionSocketFactory f = new SSLConnectionSocketFactory(sslContext, , SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
//                httpClient = HttpClients.custom().setSSLSocketFactory(f).build();


                HttpGet httpget = new HttpGet("https://52.49.91.111:8443/ghost");
//                httpget.setProtocolVersion(HttpVersion.HTTP_1_0);
//                httpget.setProtocolVersion(new ProtocolVersion("HTTP", 2, 0));
                httpget.addHeader(HttpHeaders.RANGE, "bytes=" + pos + "-8120");
                httpget.addHeader(HttpHeaders.ACCEPT_ENCODING, "identity");
                System.out.println("Executing request (" + pos + ") " + httpget.getRequestLine());
                // Create a custom response handler
                ResponseHandler<String> responseHandler = response -> {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200) {
                        HttpEntity entity = response.getEntity();
                        if (entity == null) {
                            return null;
                        }
                        fastCopy(entity.getContent(), fileOutputStream);
                        fileOutputStream.flush();
                        out.println();
                        out.flush();
                        return null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                };
                String responseBody = httpClient.execute(httpget, responseHandler);
                System.out.println("----------------------------------------");
                System.out.println(responseBody);
            }
        }
        fileOutputStream.close();
    }
}
