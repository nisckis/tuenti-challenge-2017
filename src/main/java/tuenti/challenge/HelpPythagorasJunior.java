package tuenti.challenge;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class HelpPythagorasJunior {

    private static String fileName = "HelpPythagorasJunior-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ": ");
            new HelpPythagorasJunior().solve();
            out.println();
        }
        in.close();
        out.close();
    }

    private void solve() {
        int N = in.nextInt();
        long[] sides = new long[N];
        for (int side = 0; side < N; side++) {
            sides[side] = in.nextLong();
        }
        in.nextLine();
        Arrays.sort(sides);
        int leftPos = 0;
        int middlePos = 1;
        int rightPos = 2;
        long minimum = Long.MAX_VALUE;
        while (leftPos + 2 < N) {
            long left = sides[leftPos];
            if (3 * left > minimum) {
                break;
            }
            long middle = sides[middlePos];
            long right = sides[rightPos];
            long sum = left + middle + right;
            if (sum > minimum) {
                // GREATER - IGNORE UNTIL NEXT DIFFERENT LEFT NUMBER
                do {
                    leftPos++;
                    if (leftPos + 2 == N) {
                        break;
                    }
                } while (sides[leftPos] == left);
                middlePos = leftPos + 1;
                rightPos = leftPos + 2;
                continue;
            }
            if (left + middle > right) {
                // TRIANGLE FOUND
                if (sum < minimum) {
                    // IS NEW MINIMUM
                    minimum = sum;
                }
                // IGNORE UNTIL NEXT DIFFERENT LEFT NUMBER
                do {
                    leftPos++;
                    if (leftPos + 2 == N) {
                        break;
                    }
                } while (sides[leftPos] == left);
                middlePos = leftPos + 1;
                rightPos = leftPos + 2;
                continue;
            }
            // NOT A TRIANGLE
            middlePos++;
            rightPos = middlePos + 1;
            if (rightPos >= N) {
                leftPos++;
                middlePos = leftPos + 1;
                rightPos = leftPos + 2;
            }
        }
        if (minimum == Long.MAX_VALUE) {
            out.print("IMPOSSIBLE");
        } else {
            out.print(minimum);
        }
    }
}
