package tuenti.challenge;

import com.google.common.primitives.Ints;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Locale;
import java.util.Scanner;

public class TheSupremeScalextricArchitect {

    private static String fileName = "TheSupremeScalextricArchitect-submit";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        in = new Scanner(new FileReader(inputFileName));
        out = new PrintWriter(outputFileName);
        int tests = in.nextInt();
        in.nextLine();
        for (int t = 1; t <= tests; t++) {
            out.print("Case #" + t + ": ");
            new TheSupremeScalextricArchitect().solve();
            out.println();
        }
        in.close();
        out.close();
    }

    private int solveRemaining(int S, int C, int D) {
        int initialS = S;
        int numTracks = 0;
        int C8 = C >> 3;
        numTracks += (C8 << 3);
        C = C % 8;
        int C4 = C >> 2;
        boolean C4S2 = false;
        if (C4 > 0) {
            if (S >= 2) {
                // 4C + 2S
                numTracks += 6;
                S -= 2;
                C4S2 = true;
            } else if (D >= 1) {
                // 4C + 1D
                numTracks += 5;
                D--;
            }
        }
        C = C % 4;
        int C2 = C >> 1;
        if (C2 > 0) {
            if (S >= 2) {
                // 2C + 2S
                numTracks += 4;
                S -= 2;
            } else if ((D >= 1) && (C4S2)) {
                // 2C + 1D
                numTracks += 3;
                D--;
            }
        }
        C = C % 2;
        int D2 = D >> 1;
        numTracks += (D2 << 1);
        D = D % 2;
        if ((D > 0) && (S >= 2)) {
            numTracks += 3;
            D = 0;
            S -= 2;
        }
        int S2 = S >> 1;
        numTracks += (S2 << 1);
        S = S % 2;
        {
            if ((D > 0) && (initialS >= 2)) {
                S2 = initialS >> 1;
                numTracks += Ints.min(D, S2);
            }
        }
        return numTracks;
    }

    private int solveStartingWith4C(int S, int C, int D) {
        int numTracks = 0;
        C -= 4;
        numTracks += 4;
        numTracks += solveRemaining(S, C, D);
        return numTracks;
    }

    private int solveStartingWith8C2S(int S, int C, int D) {
        int numTracks = 0;
        if ((C < 8) || (S < 2)) {
            return 0;
        }
        C -= 8;
        S -= 2;
        numTracks += 10;
        numTracks += solveRemaining(S, C, D);
        return numTracks;
    }

    private int solveStartingWith8C2D(int S, int C, int D) {
        int numTracks = 0;
        if ((C < 8) || (D < 2)) {
            return 0;
        }
        C -= 8;
        D -= 2;
        numTracks += 10;
        numTracks += solveRemaining(S, C, D);
        return numTracks;
    }

    private int solveStartingWith10C2S(int S, int C, int D) {
        int numTracks = 0;
        if ((C < 10) || (S < 2)) {
            return 0;
        }
        C -= 10;
        S -= 2;
        numTracks += 12;
        numTracks += solveRemaining(S, C, D);
        return numTracks;
    }

    private int solveStartingWith12C2S(int S, int C, int D) {
        int numTracks = 0;
        if ((C < 12) || (S < 2)) {
            return 0;
        }
        C -= 12;
        S -= 2;
        numTracks += 14;
        numTracks += solveRemaining(S, C, D);
        return numTracks;
    }

    private int solveStartingWith12C1D(int S, int C, int D) {
        int numTracks = 0;
        if ((C < 12) || (D < 1)) {
            return 0;
        }
        C -= 12;
        D--;
        numTracks += 13;
        numTracks += solveRemaining(S, C, D);
        return numTracks;
    }

    private void solve() {
        int S = in.nextInt();
        int C = in.nextInt();
        int D = in.nextInt();
        in.nextLine();
        if (C < 4) {
            out.print(0);
            return;
        }
        int with4C = solveStartingWith4C(S, C, D);
        int with8C2S = solveStartingWith8C2S(S, C, D);
        int with8C2D = solveStartingWith8C2D(S, C, D);
        int with10C2S = solveStartingWith10C2S(S, C, D);
        int with12C2S = solveStartingWith12C2S(S, C, D);
        int with12C1D = solveStartingWith12C1D(S, C, D);
        int max = Ints.max(with4C, with8C2S, with8C2D, with10C2S, with12C2S, with12C1D);
        out.print(max);
    }

}
