package tuenti.challenge;

import com.google.common.base.CharMatcher;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.io.BaseEncoding;
import com.google.common.io.CharSource;
import okhttp3.*;
import okio.ByteString;
import org.apache.commons.codec.Charsets;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordSoupChallenge extends WebSocketListener {

    private static String fileName = "WordSoupChallenge";
    private static String inputFileName = fileName + ".in";
    private static String outputFileName = fileName + ".out";
    private static Scanner in;
    private static PrintWriter out;

    public static final String GAMEOVER = "ZC5ib2R5LmlubmVySFRNTD0nPGRpdiBzdHlsZT0iaGVpZ2h0OjEwMHZoO2Rpc3BsYXk6ZmxleDtqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyO2FsaWduLWl0ZW1zOmNlbnRlcjtmb250LXNpemU6NTBweCI+R0FNRSBPVkVSPC9kaXY+Jyx3Lm9uY2xvc2U9bnVsbDs=";

    private static final int INITIALIZING = 0;
    private static final int INITIALIZED = 1;
    private static final int SOLVED = 2;
    private static final int FINISHED = Integer.MAX_VALUE;

    private int state = 0;
    private int numRows;
    private int numColums;
    private int level;
    private char[][] puzzle;
    private String[] words;
    private int numAcceptedWords;

    public static void main(String[] args) throws FileNotFoundException {
        Locale.setDefault(Locale.ENGLISH);
        out = new PrintWriter(outputFileName);
        new WordSoupChallenge().run();
    }

    private void run() {
        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(0, TimeUnit.MILLISECONDS)
                .build();

        Request request = new Request.Builder()
//                .url("ws://echo.websocket.org")
                .url("ws://52.49.91.111:3636/word-soup-challenge")
                .build();
        client.newWebSocket(request, this);

        // Trigger shutdown of the dispatcher's executor so this process can exit cleanly.
        client.dispatcher().executorService().shutdown();
    }

    @Override
    public void onOpen(WebSocket webSocket, Response response) {
    }

    @Override
    public void onMessage(WebSocket webSocket, String text) {
        System.out.println("MESSAGE: " + text);
        synchronized (this) {
            if (state == INITIALIZING) {
                try {
                    String message = BaseEncoding.base64().decodingSource(CharSource.wrap(text)).asCharSource(Charsets.UTF_8).read();
                    System.out.println(message);
                    Pattern pattern = Pattern.compile("^\\{const t=d.body,n=(?<n>\\d+),e=(?<e>\\d+),o=(?<o>\\d+),s=\\[(?<s>\\[.*])],c=\\[(?<c>\".*\")];.*$");
                    Matcher matcher = pattern.matcher(message);
                    boolean matches = matcher.matches();
                    System.out.println(matches);
                    if (!matches) {
                        state = -1;
                        throw new RuntimeException("UNABLE TO PARSE INITIAL DATA");
                    }
                    String n = matcher.group("n");
                    String e = matcher.group("e");
                    String o = matcher.group("o");
                    String s = matcher.group("s");
                    String c = matcher.group("c");
                    System.out.println(n);
                    System.out.println(e);
                    System.out.println(o);
                    System.out.println(s);
                    System.out.println(c);
                    numRows = Integer.valueOf(n);
                    System.out.println(numRows);
                    numColums = Integer.valueOf(e);
                    System.out.println(numColums);
                    level = Integer.valueOf(o);
                    System.out.println(level);
                    List<String> rows = Splitter.on(CharMatcher.anyOf("[\",]")).omitEmptyStrings().trimResults().splitToList(s);
                    System.out.println(rows);
                    puzzle = new char[numColums][numRows];
                    for (int numRow = 0; numRow < numRows; numRow++) {
                        for (int numCol = 0; numCol < numColums; numCol++) {
                            puzzle[numRow][numCol] = rows.get((numRow * numColums) + numCol).charAt(0);
                        }
                    }
                    System.out.println(Arrays.deepToString(puzzle));
                    words = Splitter.on(CharMatcher.anyOf("\",]")).omitEmptyStrings().trimResults().splitToList(c).toArray(new String[0]);
                    System.out.println(Arrays.toString(words));
                } catch (IOException e) {
                    state = -1;
                    throw new RuntimeException("UNABLE TO PARSE INITIAL DATA", e);
                }
                state = INITIALIZED;
                PuzzleSolver solver = new PuzzleSolver(puzzle, words);
                solver.solve();
                System.out.println(solver);
                for (PuzzleSolver.Coord coord : solver.solutions.values()) {
                    System.out.println(coord);
                    String location = Joiner.on('-').join(coord.startX, coord.startY, coord.endX, coord.endY);
                    System.out.println(location);
                    String message = location + "-" + m(location);
                    System.out.println(message);
                    String encodedMessage = BaseEncoding.base64().encode(message.getBytes(Charsets.UTF_8));
                    System.out.println(encodedMessage);
                    webSocket.send(encodedMessage);
                }
            } else if (state == INITIALIZED) {
                try {
                    String message = BaseEncoding.base64().decodingSource(CharSource.wrap(text)).asCharSource(Charsets.UTF_8).read();
                    System.out.println(message);
                    numAcceptedWords++;
                    if (numAcceptedWords == words.length) {
                        state = SOLVED;
                    }
                } catch (IOException e) {
                    state = -1;
                    throw new RuntimeException("UNABLE TO PARSE ACCEPTED WORD DATA");
                }
            } else if (state == SOLVED) {
                try {
                    String message = BaseEncoding.base64().decodingSource(CharSource.wrap(text)).asCharSource(Charsets.UTF_8).read();
                    System.out.println(message);
                    Pattern pattern = Pattern.compile("^.*<p>Your token for (?<phase>.*?)</p>.*?<p>(?<token>.*?)</p>.*$", Pattern.DOTALL + Pattern.MULTILINE);
                    Matcher matcher = pattern.matcher(message);
                    boolean matches = matcher.matches();
                    System.out.println(matches);
                    if (!matches) {
                        state = -1;
                        throw new RuntimeException("UNABLE TO GET TOKEN");
                    }
                    String phase = matcher.group("phase");
                    String token = matcher.group("token");
                    System.out.println(phase);
                    System.out.println(token);
                    out.println(phase);
                    out.println(token);
                    if (level >= 2) {
                        state = FINISHED;
                        webSocket.close(1000, "Finished");
                        return;
                    }
                    webSocket.send("play hard");
                    state = INITIALIZING;
                    numAcceptedWords = 0;
                } catch (IOException e) {
                    state = -1;
                    throw new RuntimeException("UNABLE TO PARSE SOLVED DATA");
                }
            }
        }
    }

    private String m(String location) {
        int s = 0;
        String c = location.concat("-saltbae");
        if (c.isEmpty()) {
            return "0";
        }
        for (int n = 0, o = c.length(); n < o; n++) {
            int e = c.charAt(n);
            s = (s << 5) - s + e;
            s |= 0;
        }
        return String.valueOf(Math.abs(s));
    }

    @Override
    public void onMessage(WebSocket webSocket, ByteString bytes) {
        System.out.println("MESSAGE: " + bytes.hex());
    }

    @Override
    public void onClosing(WebSocket webSocket, int code, String reason) {
        webSocket.close(1000, null);
        System.out.println("CLOSE: " + code + " " + reason);
        out.close();
    }

    @Override
    public void onFailure(WebSocket webSocket, Throwable t, Response response) {
        t.printStackTrace();
    }

    private class Grid {
        private char[][] grid;

        Grid(char[][] grid) {
            this.grid = grid;
        }

        public int height() {
            return grid.length;
        }

        public int width() {
            return grid[0].length;
        }

        public char at(int x, int y) {
            if (x >= 0 && x < width() && y >= 0 && y < height()) {
                return grid[y][x];
            }
            return Character.UNASSIGNED;
        }
    }

    private class PuzzleSolver {
        private WordSoupChallenge.Grid grid;
        private String[] words;
        private Map<String, Coord> solutions = new HashMap<>();

        class Coord {
            int startX, startY, endX, endY;

            Coord(int startX, int startY, int endX, int endY) {
                this.startX = startX;
                this.startY = startY;
                this.endX = endX;
                this.endY = endY;
            }

            @Override
            public String toString() {
                return String.valueOf(startX) + "x" + startY + " to " + endX + "x" + endY;
            }
        }

        PuzzleSolver(char[][] grid, String[] words) {
            this.grid = new Grid(grid);
            this.words = words;
        }

        public void solve() {
            for (String word : words)
                findWord(word);
        }

        private void findWord(String word) {
            char firstLetter = word.charAt(0);

            for (int y = 0; y < grid.height(); y++) {
                for (int x = 0; x < grid.width(); x++) {
                    if (grid.at(x, y) == firstLetter) {
                        if (this.checkLeft(x, y, word))
                            solutions.put(word, this.new Coord(x, y, (x - word.length() + 1), y));
                        else if (this.checkRight(x, y, word))
                            solutions.put(word, this.new Coord(x, y, (x + word.length() - 1), y));
                        else if (this.checkDown(x, y, word))
                            solutions.put(word, this.new Coord(x, y, x, (y + word.length() - 1)));
                        else if (this.checkUp(x, y, word))
                            solutions.put(word, this.new Coord(x, y, x, (y - word.length() + 1)));
                        else if (this.checkLeftUp(x, y, word))
                            solutions.put(word, this.new Coord(x, y, (x - word.length() + 1), (y - word.length() + 1)));
                        else if (this.checkRightDown(x, y, word))
                            solutions.put(word, this.new Coord(x, y, (x + word.length() - 1), (y + word.length() - 1)));
                        else if (this.checkLeftDown(x, y, word))
                            solutions.put(word, this.new Coord(x, y, (x - word.length() + 1), (y + word.length() - 1)));
                        else if (this.checkRightUp(x, y, word))
                            solutions.put(word, this.new Coord(x, y, (x + word.length() - 1), (y - word.length() + 1)));
                    }
                }
            }
        }

        private boolean checkLeft(int x, int y, String word) {
            if ((x + 1) - word.length() < 0) {
                return false;
            }

            int index = x;
            for (char letter : word.toCharArray()) {
                if (grid.at(index, y) != letter)
                    return false;
                index--;
            }

            return true;
        }

        private boolean checkRight(int x, int y, String word) {
            if (x + word.length() > grid.width())
                return false;

            int index = x;
            for (char letter : word.toCharArray()) {
                if (grid.at(index, y) != letter)
                    return false;
                index++;
            }

            return true;
        }

        private boolean checkDown(int x, int y, String word) {
            if (y + word.length() > grid.height())
                return false;

            int index = y;
            for (char letter : word.toCharArray()) {
                if (grid.at(x, index) != letter)
                    return false;
                index++;
            }

            return true;
        }

        private boolean checkUp(int x, int y, String word) {
            if ((y + 1) - word.length() < 0)
                return false;

            int index = y;
            for (char letter : word.toCharArray()) {
                if (grid.at(x, index) != letter)
                    return false;
                index--;
            }

            return true;
        }

        private boolean checkLeftUp(int x, int y, String word) {
            if ((y + 1) - word.length() < 0 || (x + 1) - word.length() < 0)
                return false;

            int indexX = x, indexY = y;
            for (char letter : word.toCharArray()) {
                if (grid.at(indexX, indexY) != letter)
                    return false;
                indexX--;
                indexY--;
            }

            return true;
        }

        private boolean checkRightDown(int x, int y, String word) {
            if (y + word.length() > grid.height() || x + word.length() > grid.width())
                return false;

            int indexX = x, indexY = y;
            for (char letter : word.toCharArray()) {
                if (grid.at(indexX, indexY) != letter)
                    return false;
                indexX++;
                indexY++;
            }

            return true;
        }

        private boolean checkLeftDown(int x, int y, String word) {
            if (y + word.length() > grid.height() || (x + 1) - word.length() < 0)
                return false;

            int indexX = x, indexY = y;
            for (char letter : word.toCharArray()) {
                if (grid.at(indexX, indexY) != letter)
                    return false;
                indexX--;
                indexY++;
            }

            return true;
        }

        private boolean checkRightUp(int x, int y, String word) {
            if ((y + 1) - word.length() < 0 || x + word.length() > grid.width())
                return false;

            int indexX = x, indexY = y;
            for (char letter : word.toCharArray()) {
                if (grid.at(indexX, indexY) != letter)
                    return false;
                indexX++;
                indexY--;
            }

            return true;
        }

        @Override
        public String toString() {
            StringBuilder output = new StringBuilder();
            output.append("Words: ");

            for (String word : words)
                output.append(word).append(" ");

            output.append("\n\n");

            for (int y = 0; y < grid.height(); y++) {
                for (int x = 0; x < grid.width(); x++)
                    output.append(grid.at(x, y)).append(" ");
                output.append("\n");
            }

            Set<Map.Entry<String, Coord>> entrySet = solutions.entrySet();

            for (Map.Entry<String, Coord> entry : entrySet) {
                Coord coord = entry.getValue();
                output.append(entry.getKey()).append(": ").append(coord.toString());
                output.append("\n");
            }

            return output.toString();
        }
    }
}
