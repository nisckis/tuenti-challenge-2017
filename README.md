# TUENTI CHALLENGE 7
*****
Repository containing my solutions for [Tuenti Challenge 7][tc7] using Java 8.

[Tuenti Challenge][tc7] is a coding competition where you need a mixture of programming skills, algorithm knowledge, intuition and luck.

Challenges are presented sequentially, but you are given the option of skipping up to two problems. Furthermore, this year you could not do any skip once reached Challenge 12.

I finished correctly the first 10 challenges, but being honest I was not sure that Challenge 9 was going to be sucessful because I was *not* 100% sure that I had every possible case controlled :flushed:.

When I get some more spare time I'll write solutions for the other challenges too.

I finished [25th of 1490 participants][stats], which I think is my best mark on these four years, so I got my :shirt: prize :grinning:. Unfortunately I can't verify it because you can't see stats from previous years :disappointed_relieved:

[Write-ups][wiki] are available at the repository wiki, so I'm learning [Markdown][] in the process. Right now they are in Spanish only but I'll write them in English ASAP.

[tc7]: https://contest.tuenti.net/Info/about "Tuenti Challenge 7 Info"
[wiki]: https://bitbucket.org/nisckis/tuenti-challenge-2017/wiki/Home "Wiki"
[markdown]: https://daringfireball.net/projects/markdown/syntax "MarkDown"
[stats]: http://web.archive.org/web/20170509172910/https%3A%2F%2Fcontest.tuenti.net%2Fstats "Ranking"
